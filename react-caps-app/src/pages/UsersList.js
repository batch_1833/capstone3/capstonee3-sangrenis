import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";


import Swal from "sweetalert2";

export default function UsersList(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	const [allUsers, setAllUsers] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const fetchData = () =>{
		// Get all products in the database
		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>					
						<td>{user.isAdmin ? "Admin" : "User"}</td>
						<td>
							{
								// We use conditional rendering to set which button should be visible based on the product status (admin/user)
								(user.isAdmin)
								?	
								 	// A button to change the product status to "Inactive"
									<Button variant="danger" size="sm" >Admin</Button>
								:
									<>
										{/* A button to change the product status to "Admin"*/}
										<Button variant="success" size="sm" onClick ={() => notAdmin(user._id)}>Make Admin</Button>
										{/* A button to edit a specific product*/}
										
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}


	//Making the user an admin
	const notAdmin = (userId, userName) =>{
		console.log(userId);
		console.log(userName);

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: `Update Successful!`,
					icon: "success",
					text: `${userName} is now an admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all products in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all products.
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2" >Add Product</Button>
				<Button as={Link} to="/admin" variant="danger" size="lg" className="mx-2" >Products</Button>
				<Button as={Link} to="/allUsers" variant="secondary" size="lg" className="mx-2" >Users List</Button>
				<Button as={Link} to="/orders" variant="success" size="lg" className="mx-2" >Orders</Button>
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>Users ID</th>
		         <th>First Name</th>
		         <th>Last Name</th>
		         <th>Email</th>
		         <th>Phone Number</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
