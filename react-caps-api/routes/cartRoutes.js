const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const auth = require("../auth");


// View Cart
router.get('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	cartController.getCart(userData).then(resultFromController => {
		res.send(resultFromController)
	})
});

// Add to Cart
router.post('/', auth.verify, (req, res) => {

	const data = {
		userData: auth.decode(req.headers.authorization),
		reqBody: req.body
		};

	cartController.addToCart(data).then(resultFromController => {
		res.send(resultFromController)
	})
});

// Delete Cart
router.delete('/delete', auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;

	cartController.deleteCart(userId).then(resultFromController => {
		res.send(resultFromController)
	})
});

// remove a product inside the cart
router.delete('/:productId', auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	};

	cartController.removeProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
});

module.exports = router;