const Cart = require('../models/Cart');
const User = require('../models/User');
const Product = require('../models/Product');

module.exports.getCart = (userData) => {

	return Cart.find({userId: userData.id}).then(result => {
		return result;
	})
}

module.exports.addToCart = async (data) => {
	const {productId, quantity} = data.reqBody;

	// findOne returns null (falsy) if nothing is found
	let cart = await Cart.findOne({userId: data.userData.id});
	let product = await Product.findOne({_id: productId});

	if(!product) {
		return ('Empty Product');
	}
	
	const {price, name} = product;

	if(cart) {
		let productIndex = cart.products.findIndex(p => p.productId == productId);


		if(productIndex > -1) {
			let productItem = cart.products[productIndex]; 
			productItem.quantity += quantity;
			cart.products[productIndex] = productItem;
		}
		else {
			cart.products.push({productId: productId, name: name, quantity: quantity, price: price})
		}
		cart.bill += price * quantity;

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})

	}
	else {

		cart = new Cart({
			userId: data.userData.id,
			products: [{productId: productId, name: name, quantity: quantity, price: price}],
			bill: price * quantity
		});

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})
	}
}

module.exports.deleteCart = (userId) => {
	return Cart.deleteOne({userId: userId}).then(result => {
		return result;
	})
}


module.exports.removeProduct = async (data) => {
	const {userId, productId} = data;
	let cart = await Cart.findOne({userId: userId});
	let productIndex = cart.products.findIndex(p => p.productId == productId);

	// Product is in the cart
	if(productIndex > -1) {
		let productItem = cart.products[productIndex];
		cart.bill -= productItem.quantity*productItem.price;
        cart.products.splice(productIndex,1);
	}
	return cart.save().then((result, err) => {
		if(err) {
			return false;
		}
		else {
			return result;
		}
	})

}

