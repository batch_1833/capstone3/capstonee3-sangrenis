const Product = require("../models/Product");

module.exports.addProduct = (reqBody) =>{

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	})

	return newProduct.save().then((product, error) =>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}


module.exports.getAllProduct = () => {
	return Product.find({}).then(result => result);
}

module.exports.getAllActive = ()=>{
	return Product.find({isActive:true}).then(result=>result)
}

module.exports.getProduct = (productId)=>{
	return Product.findById(productId).then(result => result)
}

module.exports.updateProduct =(productId, reqBody) =>{

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{

		if(error){
			return ("You dont have permission to make this action")
		}
		else{
			return ("successfully updated")
		}
	})
};

module.exports.archiveProduct = (productId, reqBody) =>{
	let updateActiveField = {
		isActive : reqBody.isActive
	}

	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		// Product is not archived
		if(error){
			return false;
		}
		// Product archived successfully
		else{
			return true
		}
	})
}